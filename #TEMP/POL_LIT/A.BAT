@echo off
zap %1
appe %1 %1.txt
sordbf %1 ll
us1r %1
dbf2txt %1
TRIM %1.txt r
wymien %1.txt . #13#10
wymien %1.txt , #13#10
wymien %1.txt ? #13#10
wymien %1.txt ! #13#10
wymien %1.txt : #13#10
wymien %1.txt ; #13#10
wymien %1.txt / #13#10
wymien %1.txt - #13#10
wymien %1.txt % #13#10
wymien %1.txt #34 #13#10
wymien %1.txt #60 #13#10
wymien %1.txt #62 #13#10
sortxt %1.txt 1 N
us1w %1.txt
zap %1
appe %1 %1.txt
sordbf %1 ll
del *.bak